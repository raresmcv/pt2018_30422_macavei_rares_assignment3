package model;

import BLL.ClientBLL;
import BLL.ProductBLL;

public class Comanda {
    private int orderID,clientID,productID,orderAmount;

    public Comanda(int clientID, int productID, int orderAmount){
        this.clientID = clientID;
        this.productID = productID;
        this.orderAmount = orderAmount;
    }

    public String toString(){
        ProductBLL productBLL = new ProductBLL();
        ClientBLL clientBLL = new ClientBLL();
        Product pr = new Product();
        pr = productBLL.findProductById(productID);
        Client cl = new Client();
        cl = clientBLL.findClientByID(clientID);
        if(pr.getProductStock()-orderAmount>=0)
            return "Client "+cl.getClientName()+"("+clientID+") has purchased "+
                orderAmount+" units of product "+pr.getProductName()+"("+productID+")";
        else
            return "Cannot make order! Not enough units";
    }
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(int orderAmount) {
        this.orderAmount = orderAmount;
    }


}
