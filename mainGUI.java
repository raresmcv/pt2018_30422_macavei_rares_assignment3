package Presentation;


import model.Client;

import javax.swing.*;
        import java.awt.*;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import static javax.swing.SwingConstants.CENTER;

public class mainGUI extends JFrame {

    private JLabel welcome;
    private JButton order, product, client;

    public mainGUI() {
        this.setTitle("Welcome");
        this.setSize(300, 300);
        setResizable(false);
        this.setLocationRelativeTo(null);
        initializeView();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);

        this.setVisible(true);
    }

    private void initializeView() {
        client = new JButton();
        product = new JButton();
        order = new JButton();
        welcome = new JLabel("Warehouse Management System",SwingConstants.CENTER);

        client.setBounds(10, 150, 90, 50);
        client.setText("Client");

        product.setBounds(110, 150, 90, 50);
        product.setText("Product");

        order.setBounds(210, 150, 90, 50);
        order.setText("Comanda");

        welcome.setBounds(30,-30,250,100);
        welcome.setText("Warehouse Management System");

        client.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                ClientGUI clientGui = new ClientGUI();
            }
        });
        product.addActionListener((e)->{ProductGUI productGUI = new ProductGUI();});

        order.addActionListener((e)->{OrderGUI orderGUI = new OrderGUI();});

        this.add(order);
        this.add(product);
        this.add(client);
        this.add(welcome);
    }


    public static String[] getTableData(Object classObject)
    {
        Field[] fields =  classObject.getClass().getDeclaredFields();
        String data[]=new String[fields.length];//COLOANELE TABELULUI
        int i=0;
        for(Field field : fields)
        {
            data[i++]=field.getName();

        }
        return data;
    }
}


