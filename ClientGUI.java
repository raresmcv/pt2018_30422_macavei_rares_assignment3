package Presentation;

import BLL.ClientBLL;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientGUI extends JFrame {

    private JTable table;
    private JButton btnAdd, btnDelete, btnUpdate;
    private JScrollPane sp;
    private JTextField newNameTf;

    public JTextField getNewNameTf() {
        return newNameTf;
    }

    public void setNewNameTf(String newNameTf) {
        this.newNameTf.setText(newNameTf);
    }

    public JTextField getIndexDeleteTf() {
        return indexDeleteTf;
    }

    public void setIndexDeleteTf(String indexDeleteTf) {
        this.indexDeleteTf.setText( indexDeleteTf);
    }

    public String getUpdateNameTf() {
        return updateNameTf.getText();
    }

    public void setUpdateNameTf(String updateNameTf) {
        this.updateNameTf.setText(updateNameTf);
    }

    public int getIndexUpdateTf() {
        return Integer.parseInt(indexUpdateTf.getText());
    }

    public void setIndexUpdateTf(String indexUpdateTf) {
        this.indexUpdateTf.setText(indexUpdateTf);
    }

    private JTextField indexDeleteTf;
    private JTextField updateNameTf;
    private JTextField indexUpdateTf;
    private JButton btnOkAdd,btnOkDelete,btnOkUpdate;
    private JLabel newNameLabel,indexDeleteLabel,indexUpdateLabel,updateNameLabel;

    static ClientBLL bll = new ClientBLL();

    public ClientGUI()
    {
        this.setTitle("Client");
        this.setSize(600,600);
        setResizable(false);
        this.setLocationRelativeTo(null);

        initializeView();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        this.setVisible(true);

    }

    private void initializeView(){
        // ----- ADD----
        btnAdd = new JButton("Add");
        btnOkAdd = new JButton("OK");
        newNameLabel = new JLabel("Name");
        newNameTf = new JTextField();

        btnAdd.setBounds(10,350,50,50);
        newNameLabel.setBounds(330,20,40,30);
        newNameTf.setBounds(400,20,90,30);
        btnOkAdd.setBounds(370,50,30,30);
        // ----- -------

        //---- DELETE----
        btnDelete = new JButton("Delete");
        indexDeleteTf = new JTextField();
        btnOkDelete = new JButton("OK");
        indexDeleteLabel = new JLabel("ID");

        indexDeleteTf.setBounds(400,20,90,30);
        indexDeleteLabel.setBounds(330,20,40,30);
        btnDelete.setBounds(70,350,50,50);
        btnOkDelete.setBounds(370,50,30,30);

        //----------------

        //----- UPDATE ------
        btnUpdate = new JButton("Edit");
        btnOkUpdate = new JButton("OK");

        updateNameLabel = new JLabel("Name");
        updateNameTf = new JTextField();
        indexUpdateTf = new JTextField();
        indexUpdateLabel = new JLabel("Index");
        indexUpdateLabel.setBounds(330,20,40,30);
        indexUpdateTf.setBounds(370,20,60,30);

        updateNameLabel.setBounds(330,60,40,30);
        updateNameTf.setBounds(370,60,70,30);
        btnUpdate.setBounds(130,350,50,50);
        btnOkUpdate.setBounds(370,150,30,30);
        // --------------------
        // Reflection
        String[] tableData=mainGUI.getTableData(new Client());
        DefaultTableModel tableModel=new DefaultTableModel(tableData,0);
        Object tableRows[];
        ArrayList<Client> tableClients = bll.getAllClients();
        for(Client c : tableClients)
        {
            tableRows = new Object[]
                    {
                            c.getClientID(),
                            c.getClientName()
                    };

            tableModel.addRow(tableRows);
        }
        table = new JTable(tableModel);
        // END REFLECTION

        table.setBounds(20,20,300,300);
        sp = new JScrollPane(table);
        sp.setBounds(10,10,310,310);
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(btnAdd);
        this.add(btnDelete);

        this.add(newNameTf);
        this.add(newNameLabel);
        this.add(btnOkAdd);
        this.add(btnOkDelete);

        this.add(indexDeleteLabel);
        this.add(indexDeleteTf);

        this.add(btnUpdate);
        this.add(btnOkUpdate);
        this.add(indexUpdateLabel);
        this.add(indexUpdateTf);

        this.add(updateNameLabel);
        this.add(updateNameTf);

        this.add(sp);
        newNameTf.setVisible(false);
        newNameLabel.setVisible(false);
        btnOkAdd.setVisible(false);
        btnOkDelete.setVisible(false);
        btnOkUpdate.setVisible(false);
        indexDeleteTf.setVisible(false);
        indexDeleteLabel.setVisible(false);
        indexUpdateLabel.setVisible(false);
        indexUpdateTf.setVisible(false);
        updateNameTf.setVisible(false);
        updateNameLabel.setVisible(false);



        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newNameTf.setVisible(true);
                newNameLabel.setVisible(true);
                btnOkAdd.setVisible(true);

                btnOkDelete.setVisible(false);//btnOkDelete.setEnabled(false);
                indexDeleteTf.setVisible(false);
                indexDeleteLabel.setVisible(false);//indexDeleteLabel.setEnabled(false);

                btnOkUpdate.setVisible(false);
                indexUpdateLabel.setVisible(false);
                indexUpdateTf.setVisible(false);
                updateNameTf.setVisible(false);
                updateNameLabel.setVisible(false);
            }
        });

        btnOkAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nume = newNameTf.getText();
                ClientBLL clientBLL = new ClientBLL();
                clientBLL.insertClient(new Client(nume));
                table.setModel(populateTable());
                setNewNameTf("");
            }
        });

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOkDelete.setVisible(true);
                indexDeleteLabel.setVisible(true);
                indexDeleteTf.setVisible(true);

                newNameTf.setVisible(false);
                newNameLabel.setVisible(false);
                btnOkAdd.setVisible(false);

                btnOkUpdate.setVisible(false);
                indexUpdateLabel.setVisible(false);
                indexUpdateTf.setVisible(false);
                updateNameTf.setVisible(false);
                updateNameLabel.setVisible(false);
            }
        });

        btnOkDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ID = Integer.parseInt(indexDeleteTf.getText());
                ClientBLL bll = new ClientBLL();
                bll.deleteClientById(ID);
                table.setModel(populateTable());
                setIndexDeleteTf("");
            }
        });


        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newNameTf.setVisible(false);
                newNameLabel.setVisible(false);
                btnOkAdd.setVisible(false);

                btnOkDelete.setVisible(false);//btnOkDelete.setEnabled(false);
                indexDeleteTf.setVisible(false);
                indexDeleteLabel.setVisible(false);//indexDeleteLabel.setEnabled(false);

                btnOkUpdate.setVisible(true);
                indexUpdateLabel.setVisible(true);
                indexUpdateTf.setVisible(true);
                updateNameTf.setVisible(true);
                updateNameLabel.setVisible(true);


            }
        });



        btnOkUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            String nume = getUpdateNameTf();
            int ID =getIndexUpdateTf();
            ClientBLL bll = new ClientBLL();
            bll.update(ID,nume);
            table.setModel(populateTable());
            setIndexUpdateTf("");
            setUpdateNameTf("");


            }
        });

    }
    public JTable getTable() {
        return table;
    }
    public static DefaultTableModel populateTable(){
        String[] tableData=mainGUI.getTableData(new Client());
        DefaultTableModel tableModel=new DefaultTableModel(tableData,0);
        Object tableRows[];
        ArrayList<Client> tableClients = bll.getAllClients();
        for(Client c : tableClients)
        {
            tableRows = new Object[]
                    {
                            c.getClientID(),
                            c.getClientName()
                    };

            tableModel.addRow(tableRows);
        }

        return tableModel;
    }




}
