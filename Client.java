package model;

public class Client {
    private int clientID;
    private String clientName;

    public Client(int clientID, String clientName) {
        this.clientID = clientID;
        this.clientName = clientName;
    }
    public Client()
    {

    }
    public Client(String clientName) {
        this.clientName = clientName;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Override
    public String toString() {
        return "Client id=" + clientID + ", name=" + clientName ;
    }
}
