package BLL;

import BLL.Validators.ClientAgeValidator;
import BLL.Validators.Validators;
import DAO.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBLL {
    private List <Validators <Client>> validatorsList;

    public ClientBLL() {
        validatorsList = new ArrayList <Validators <Client>>();
        validatorsList.add(new ClientAgeValidator());

    }

    public Client findClientByID(int id) {
        Client cl = ClientDAO.findByID(id);
        if (cl == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return cl;
    }

    public int insertClient(Client client) {
        for (Validators <Client> c : validatorsList) {
            c.validate(client);
        }
        return ClientDAO.insert(client);
    }
    public void update(int id, String newName){
       ClientDAO.updateName(id,newName);
    }
    public boolean deleteClientById(int id){
       return ClientDAO.deleteById(id);
    }

    public ArrayList<Client> getAllClients()
    {
        return ClientDAO.getAllClients();
    }

}
