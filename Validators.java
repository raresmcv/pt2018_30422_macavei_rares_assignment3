package BLL.Validators;

public interface Validators<T> {
    void validate(T t);
}
