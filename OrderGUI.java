package Presentation;

import BLL.ComandaBLL;
import model.Comanda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderGUI extends JFrame {
    private JTable tableProduct,tableClient;
    private JScrollPane spProduct,spClient;
    private JButton addOrder;
    private JLabel indexClientLabel,indexProductLabel,amountLabel,descriptionLabel;
    private JTextField indexClientTf,indexProductTf,amountTf;

    public OrderGUI(){
        this.setTitle("Order");
        this.setSize(800,600);
        setResizable(false);
        this.setLocationRelativeTo(null);

        initializeView();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        this.setVisible(true);
    }

    private void initializeView() {

        tableProduct = new JTable();
        tableProduct.setModel(ProductGUI.populateTable());
        tableProduct.setBounds(20,20,300,300);
        spProduct = new JScrollPane(tableProduct);
        spProduct.setBounds(10,10,310,310);
        spProduct.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        spProduct.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(spProduct);


        tableClient = new JTable();
        tableClient.setModel(ClientGUI.populateTable());
        tableClient.setBounds(20,20,300,300);
        spClient = new JScrollPane(tableClient);
        spClient.setBounds(400,10,310,310);
        spClient.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        spClient.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(spClient);

        addOrder = new JButton("Add Order");
        indexClientLabel = new JLabel("ClientID");
        indexProductLabel = new JLabel("ProductID");
        amountLabel = new JLabel("Amount");
        indexClientTf = new JTextField();
        indexProductTf = new JTextField();
        amountTf = new JTextField();
        indexProductLabel.setBounds(0,350,70,50);
        indexProductTf.setBounds(60,350,50,50);
        indexClientLabel.setBounds(150,350,70,50);
        indexClientTf.setBounds(200,350,50,50);
        amountLabel.setBounds(0,400,50,50);
        amountTf.setBounds(60,400,50,50);
        addOrder.setBounds(250,420,90,30);
        descriptionLabel = new JLabel("");
        descriptionLabel.setBounds(0,500,600,100);
        this.add(addOrder);
        this.add(indexClientLabel);
        this.add(indexClientTf);
        this.add(indexProductLabel);
        this.add(indexProductTf);
        this.add(amountLabel);
        this.add(amountTf);
        this.add(descriptionLabel);

        addOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComandaBLL bll = new ComandaBLL();
                int cid = Integer.parseInt(indexClientTf.getText());
                int pid = Integer.parseInt(indexProductTf.getText());
                int amount = Integer.parseInt(amountTf.getText());
                Comanda comanda = new Comanda(cid,pid,amount);
                descriptionLabel.setText(comanda.toString());
                bll.insertOrder(comanda);
                tableProduct.setModel(ProductGUI.populateTable());

            }
        });

    }


}
