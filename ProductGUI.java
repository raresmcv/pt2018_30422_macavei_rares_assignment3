package Presentation;

import BLL.ProductBLL;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ProductGUI extends JFrame {

    private JTable table;
    private JButton btnAdd, btnDelete, btnUpdate;
    private JScrollPane sp;

    // ADD
    private JTextField addNameTf,addPriceTf,addStockTf;
    private JLabel addNameLabel,addPriceLabel,addStockLabel;
    // -----

    // DELETE
    private JTextField deleteIndexTf;
    private JLabel indexDeleteLabel;

    // -----

    // UPDATE
    private JTextField updateNameTf;
    private JTextField updatePriceTf;
    private JTextField updateStockTf;
    private JTextField updateIndexTf;

    public String getUpdateNameTf() {
        return updateNameTf.getText();
    }

    public void setUpdateNameTf(String updateNameTf) {
        this.updateNameTf.setText(updateNameTf);
    }

    public int getUpdatePriceTf() {
        if(updatePriceTf.getText().equals("")) return -1;
        return Integer.parseInt(updatePriceTf.getText());
    }


    public int getUpdateStockTf() {
        if(updateStockTf.getText().equals("")) return -1;
        return Integer.parseInt(updateStockTf.getText());
    }




    public int getUpdateIndexTf() {
        return Integer.parseInt(updateIndexTf.getText());
    }

    public void setUpdateIndexTf(JTextField updateIndexTf) {
        this.updateIndexTf = updateIndexTf;
    }

    private JLabel updateIndexLabel,updateNameLabel,updatePriceLabel,updateStockLabel;
    // ------

    private JButton btnOkAdd,btnOkDelete,btnOkUpdate;


    static ProductBLL bll = new ProductBLL();

    // ------ SETTERS and GETTERS
    public JTextField getAddNameTf() {
        return addNameTf;
    }

    public void setAddNameTf(String addNameTf) {
        this.addNameTf.setText(addNameTf);
    }

    public JTextField getDeleteIndexTf() {
        return deleteIndexTf;
    }

    public void setDeleteIndexTf(String deleteIndexTf) {
        this.deleteIndexTf.setText(deleteIndexTf);
    }




    // ------------------------
    public ProductGUI()
    {
        this.setTitle("Product");
        this.setSize(600,600);
        setResizable(false);
        this.setLocationRelativeTo(null);

        initializeView();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        this.setVisible(true);

    }

    private void initializeView(){
        // ----- ADD----
        btnAdd = new JButton("Add");
        btnOkAdd = new JButton("OK");
        addNameLabel = new JLabel("Name");
        addNameTf = new JTextField();
        addPriceLabel = new JLabel("Price");
        addPriceTf = new JTextField();
        addStockLabel = new JLabel("Stock");
        addStockTf = new JTextField();

        btnAdd.setBounds(10,350,50,50);
        addNameLabel.setBounds(330,20,40,30);
        addNameTf.setBounds(400,20,90,30);
        addPriceLabel.setBounds(330,70,40,30);
        addPriceTf.setBounds(400,70,90,30);
        addStockLabel.setBounds(330,120,40,30);
        addStockTf.setBounds(400,120,40,30);
        btnOkAdd.setBounds(370,250,30,30);
        // ----- -------

        //---- DELETE----
        btnDelete = new JButton("Delete");
        deleteIndexTf = new JTextField();
        btnOkDelete = new JButton("OK");
        indexDeleteLabel = new JLabel("ID");

        deleteIndexTf.setBounds(400,20,90,30);
        indexDeleteLabel.setBounds(330,20,40,30);
        btnDelete.setBounds(70,350,50,50);
        btnOkDelete.setBounds(370,50,30,30);

        //----------------

        //----- UPDATE ------
        btnUpdate = new JButton("Edit");
        btnOkUpdate = new JButton("OK");

        updateNameLabel = new JLabel("Name");
        updateNameTf = new JTextField();
        updateIndexTf = new JTextField();
        updateIndexLabel = new JLabel("Index");
        updatePriceTf = new JTextField();
        updatePriceLabel = new JLabel("Price");
        updateStockLabel = new JLabel("Stock");
        updateStockTf = new JTextField();

        updateIndexLabel.setBounds(330,20,40,30);
        updateIndexTf.setBounds(370,20,60,30);
        updateNameLabel.setBounds(330,60,40,30);
        updateNameTf.setBounds(370,60,70,30);
        updatePriceLabel.setBounds(330,100,40,30);
        updatePriceTf.setBounds(370,100,70,30);
        updateStockLabel.setBounds(330,140,40,30);
        updateStockTf.setBounds(370,140,40,30);

        btnUpdate.setBounds(130,350,50,50);
        btnOkUpdate.setBounds(370,330,30,30);
        // --------------------
        /* Reflection
        String[] tableData=mainGUI.getTableData(new Product());

        DefaultTableModel tableModel=new DefaultTableModel(tableData,0);
        Object tableRows[];
        ArrayList<Product> tableProduct = bll.getAllProducts();
        for( Product p : tableProduct)
        {
            tableRows = new Object[]
                    {
                            p.getProductID(),
                            p.getProductName(),
                            p.getProductPrice(),
                            p.getProductStock()
                    };

            tableModel.addRow(tableRows);
        }
        table = new JTable(tableModel);
        */
        //END REFLECTION
        ArrayList<Product> products = bll.getAllProducts();
        int data = Product.class.getDeclaredFields().length;
        table = populateTableReflection(products,data);

        table.setBounds(20,20,300,300);
        sp = new JScrollPane(table);
        sp.setBounds(10,10,310,310);
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        this.add(btnAdd);
        this.add(btnDelete);
        this.add(btnUpdate);

        this.add(addNameTf);
        this.add(addNameLabel);
        this.add(btnOkAdd);
        this.add(addPriceLabel);
        this.add(addPriceTf);
        this.add(addStockLabel);
        this.add(addStockTf);

        this.add(btnOkDelete);
        this.add(indexDeleteLabel);
        this.add(deleteIndexTf);


        this.add(btnOkUpdate);
        this.add(updateIndexLabel);
        this.add(updateIndexTf);
        this.add(updateNameLabel);
        this.add(updateNameTf);
        this.add(updatePriceLabel);
        this.add(updatePriceTf);
        this.add(updateStockLabel);
        this.add(updateStockTf);

        this.add(sp);

        addNameTf.setVisible(false);
        addNameLabel.setVisible(false);
        btnOkAdd.setVisible(false);
        addPriceTf.setVisible(false);
        addPriceLabel.setVisible(false);
        addStockTf.setVisible(false);
        addStockLabel.setVisible(false);

        btnOkDelete.setVisible(false);
        deleteIndexTf.setVisible(false);
        indexDeleteLabel.setVisible(false);

        updateIndexLabel.setVisible(false);
        updateIndexTf.setVisible(false);
        updateNameTf.setVisible(false);
        updateStockTf.setVisible(false);
        updateStockLabel.setVisible(false);
        updatePriceLabel.setVisible(false);
        updatePriceTf.setVisible(false);
        updateNameLabel.setVisible(false);
        btnOkUpdate.setVisible(false);



        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addNameTf.setVisible(true);
                addNameLabel.setVisible(true);
                btnOkAdd.setVisible(true);
                addPriceLabel.setVisible(true);
                addPriceTf.setVisible(true);
                addStockLabel.setVisible(true);
                addStockTf.setVisible(true);

                btnOkDelete.setVisible(false);//btnOkDelete.setEnabled(false);
                deleteIndexTf.setVisible(false);
                indexDeleteLabel.setVisible(false);//indexDeleteLabel.setEnabled(false);

                btnOkUpdate.setVisible(false);
                updateIndexLabel.setVisible(false);
                updateIndexTf.setVisible(false);
                updateNameTf.setVisible(false);
                updateNameLabel.setVisible(false);
            }
        });

        btnOkAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nume = addNameTf.getText();
                int price = Integer.parseInt(addPriceTf.getText());
                int stock = Integer.parseInt(addStockTf.getText());
                ProductBLL bll = new ProductBLL();
                bll.insertProduct(new Product(nume,price,stock));
                table.setModel(populateTable());
                setAddNameTf("");

            }
        });

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOkDelete.setVisible(true);
                indexDeleteLabel.setVisible(true);
                deleteIndexTf.setVisible(true);

                addNameTf.setVisible(false);
                addNameLabel.setVisible(false);
                btnOkAdd.setVisible(false);
                addPriceLabel.setVisible(false);
                addPriceTf.setVisible(false);
                addStockLabel.setVisible(false);
                addStockTf.setVisible(false);

                btnOkUpdate.setVisible(false);
                updateIndexLabel.setVisible(false);
                updateIndexTf.setVisible(false);
                updateNameTf.setVisible(false);
                updateNameLabel.setVisible(false);
                updatePriceLabel.setVisible(false);
                updatePriceTf.setVisible(false);
                updateStockLabel.setVisible(false);
                updateStockTf.setVisible(false);
            }
        });

        btnOkDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ID = Integer.parseInt(deleteIndexTf.getText());
                ProductBLL bll = new ProductBLL();
                bll.deleteProductById(ID);

                table.setModel(populateTable());
                setDeleteIndexTf("");
            }
        });


        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addNameTf.setVisible(false);
                addNameLabel.setVisible(false);
                btnOkAdd.setVisible(false);
                addPriceLabel.setVisible(false);
                addPriceTf.setVisible(false);
                addStockLabel.setVisible(false);
                addStockTf.setVisible(false);

                btnOkDelete.setVisible(false);//btnOkDelete.setEnabled(false);
                deleteIndexTf.setVisible(false);
                indexDeleteLabel.setVisible(false);//indexDeleteLabel.setEnabled(false);

                btnOkUpdate.setVisible(true);
                updateIndexLabel.setVisible(true);
                updateIndexTf.setVisible(true);
                updateNameTf.setVisible(true);
                updateNameLabel.setVisible(true);
                updatePriceLabel.setVisible(true);
                updatePriceTf.setVisible(true);
                updateStockLabel.setVisible(true);
                updateStockTf.setVisible(true);


            }
        });



        btnOkUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nume = getUpdateNameTf();
                int price = getUpdatePriceTf();
                int stock = getUpdateStockTf();
                int ID =getUpdateIndexTf();
                ProductBLL bll = new ProductBLL();
                Product pr = new Product();
                pr = bll.findProductById(ID);
                if(nume.equals("")) nume = pr.getProductName();
                if(price==-1) price = pr.getProductPrice();
                if(stock==-1) stock = pr.getProductStock();

                bll.update(ID,nume,price,stock);
                table.setModel(populateTable());
                setUpdateNameTf("");
                //setUpdatePriceTf(0);
                //setUpdateStockTf(0);



            }
        });

    }
    public JTable getTable() {
        return table;
    }

    public static DefaultTableModel populateTable(){
        String[] tableData=mainGUI.getTableData(new Product());
        DefaultTableModel tableModel=new DefaultTableModel(tableData,0);
        Object tableRows[];
        ArrayList<Product> tableProducts = bll.getAllProducts();
        for(Product p : tableProducts)
        {
            tableRows = new Object[]
                    {
                            p.getProductID(),
                            p.getProductName(),
                            p.getProductPrice(),
                            p.getProductStock()
                    };

            tableModel.addRow(tableRows);
        }

        return tableModel;
    }

    public static JTable populateTableReflection(ArrayList<?> obj,int noAttributes){

        ArrayList<String>rows=new ArrayList<String>();
        Object[]coloane=new Object[noAttributes];

        int nrColoane=0;
        int nrRow=0;

        for (Object ob: obj) {

            int lungime=ob.getClass().getDeclaredFields().length;

            for (Field f : ob.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                Object value = null;

                try {
                    value = f.get(ob);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if( nrColoane < lungime){

                    coloane[nrColoane++]=f.getName();
                }
                rows.add(value.toString());

            }
        }

        Object [][]rand= new Object[obj.size()][nrColoane];

        for(int rw = 0; rw < obj.size(); rw++) {
            for(int r = 0 ;r < nrColoane; r++) {
                if(nrRow < rows.size())
                    rand[rw][r]=rows.get(nrRow);
                nrRow++;
            }
        }
        JTable table=new JTable(rand,coloane);
        return table;
    }





}
