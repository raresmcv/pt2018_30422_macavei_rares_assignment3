package DAO;

import BLL.ClientBLL;
import BLL.ProductBLL;
import connection.ConnectionFactory;
import model.Client;
import model.Comanda;
import model.Product;

import java.sql.*;
import java.util.logging.*;

public class ComandaDAO {
    protected static final Logger LOGGER = Logger.getLogger(Comanda.class.getName());
    private static final String insertStatementString = "INSERT INTO Comanda(clientID,productID,orderAmount)" + " VALUES(?,?,?)";
    public static int insertOrder(Comanda comanda){
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        ProductBLL bllP = new ProductBLL();
        Product pr =bllP.findProductById(comanda.getProductID());

        ClientBLL bllC = new ClientBLL();
        Client cl = bllC.findClientByID(comanda.getClientID());
        int remaining = pr.getProductStock()- comanda.getOrderAmount();
        if(remaining>=0) {
            try {
                insertStatement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
                insertStatement.setInt(1, comanda.getClientID());
                insertStatement.setInt(2, comanda.getProductID());
                insertStatement.setInt(3, comanda.getOrderAmount());
                insertStatement.executeUpdate();

                ResultSet rs = insertStatement.getGeneratedKeys();
                if( rs.next()){
                    insertedId = rs.getInt(1);
                }

                bllP.update(pr.getProductID(),pr.getProductName(),pr.getProductPrice(),remaining);
                System.out.println(comanda.toString());

            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "ComandaDAO: insert " + e.getMessage());
            } finally {
                ConnectionFactory.close(connection);
                ConnectionFactory.close(insertStatement);

            }
        }
        return insertedId;
    }

}
