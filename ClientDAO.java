package DAO;


import connection.ConnectionFactory;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDAO {
    protected static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static final String insertStatementString = "INSERT INTO Client(clientName)" + " VALUES(?)";
    private static final String findStatementString = "SELECT * FROM warehouse.Client where clientID = ?";
    private static final String findAllStatementString = "SELECT * FROM warehouse.Client";
    private static final String deleteStatementString = "DELETE FROM warehouse.Client where clientID = ?";
    private static final String updateStatementString = "UPDATE warehouse.Client SET clientName=? where clientID = ?";


    public static Client findByID(int clientID) {
        Client toReturn = null;
        Connection connection = ConnectionFactory.getConnection();
        ResultSet rs = null;
        PreparedStatement findStatement = null;
        try {
            findStatement = connection.prepareStatement(findStatementString);
            findStatement.setLong(1, clientID);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("clientName");
            //String address = rs.getString("address");
            //String email = rs.getString("email");
            //int age = rs.getInt("age");
            toReturn = new Client(clientID, name);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(connection);

        }
        return toReturn;
    }

    public static ArrayList <Client> getAllClients() {
        ArrayList <Client> list = new ArrayList <>();
        Connection connection = ConnectionFactory.getConnection();
        ResultSet rs = null;
        PreparedStatement getStatement = null;
        try {
            getStatement = connection.prepareStatement(findAllStatementString);
            rs = getStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("clientID");
                String name = rs.getString("clientName");
                Client cl = new Client(id, name);
                list.add(cl);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(getStatement);
            ConnectionFactory.close(rs);
        }
        return list;
    }

    public static int insert(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, client.getClientName());
            //insertStatement.setString(2, student.getAddress());
            //insertStatement.setString(3, student.getEmail());
            //insertStatement.setInt(4, student.getAge());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static boolean deleteById(int id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);
            deleteStatement.executeUpdate();
            System.out.println("Item has been deleted");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
            return false;

        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

        return true;
    }

    public static void updateName(int id,String newName) {
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement updateSt = null;
        String updateQuery="UPDATE warehouse.Client SET clientName="+'"'+newName+'"'+"WHERE clientId="+id;
        try
        {
            updateSt=(PreparedStatement) con.prepareStatement(updateQuery);
            updateSt.executeUpdate();
            System.out.println("Client  "+id+" updated");

        }catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }

    }

}
