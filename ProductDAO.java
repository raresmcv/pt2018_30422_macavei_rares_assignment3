package DAO;

import BLL.ProductBLL;
import connection.ConnectionFactory;
import model.Client;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDAO {
    protected static final Logger LOGGER = Logger.getLogger(Product.class.getName());
    private static final String insertStatementString = "INSERT INTO Product(productName,productPrice,productStock)" + " VALUES(?,?,?)";
    private static final String findStatementString = "SELECT * FROM warehouse.Product where productID = ?";
    private static final String findAllStatementString = "SELECT * FROM warehouse.Product";
    private static final String deleteStatementString = "DELETE FROM warehouse.Product where productID = ?";
    private static final String updateStatementString = "UPDATE warehouse.Product SET productName=?, "
            + " productPrice = ?," +" productStock = ?"+ " where productID = ? ";


    public static Product findByID(int productID) {
        Product toReturn = null;
        Connection connection = ConnectionFactory.getConnection();
        ResultSet rs = null;
        PreparedStatement findStatement = null;
        try {
            findStatement = connection.prepareStatement(findStatementString);
            findStatement.setLong(1,productID);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("productName");
            int price = rs.getInt("productPrice");
            int stock = rs.getInt("productStock");
            toReturn = new Product(productID,name,price,stock);

        } catch(SQLException e){
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(connection);
        }
        return toReturn;
    }

    public static ArrayList<Product> getAllProducts(){
        ArrayList<Product> list = new ArrayList <>();
        Connection connection = ConnectionFactory.getConnection();
        ResultSet rs = null;
        PreparedStatement getStatement = null;

        try{
            getStatement = connection.prepareStatement(findAllStatementString);
            rs = getStatement.executeQuery();
            while(rs.next()){
                int id = rs.getInt("productID");
                String name = rs.getString("productName");
                int price = rs.getInt("productPrice");
                int stock = rs.getInt("productStock");

                Product pr = new Product(id,name,price,stock);
                list.add(pr);

            }

        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getStatement);
        }
        return list;
    }

    public static  int insert(Product pr){
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, pr.getProductName());
            insertStatement.setInt(2,pr.getProductPrice());
            insertStatement.setInt(3,pr.getProductStock());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static boolean deleteById(int id){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);
            deleteStatement.executeUpdate();
            System.out.println("Item has been deleted");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
            return false;

        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

        return true;
    }

    public static void updateById(int id,String name,int price,int stock){
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement updateSt = null;
        ProductBLL bll = new ProductBLL();
        try{
            updateSt = connection.prepareStatement(updateStatementString);
            updateSt.setString(1,name);
            updateSt.setInt(2,price);
            updateSt.setInt(3,stock);
            updateSt.setInt(4,id);
            updateSt.executeUpdate();
            System.out.println("Product "+ id+ "updated");

        }catch(SQLException e){
            System.out.println(e.getMessage());
        }finally {
            ConnectionFactory.close(connection);

        }
    }
}
