package BLL;

import DAO.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ProductBLL {
    public Product findProductById(int id){
        Product pr = ProductDAO.findByID(id);
        if(pr == null){
            throw new NoSuchElementException("The product with id = "+id+" was not found");
        }
        return pr;
    }

    public ArrayList<Product> getAllProducts(){
        return ProductDAO.getAllProducts();
    }

    public int insertProduct(Product pr){
        return ProductDAO.insert(pr);
    }

    public boolean deleteProductById(int id){
        return ProductDAO.deleteById(id);
    }
    public void update(int id,String name,int price,int stock){
        ProductDAO.updateById(id,name,price,stock);
    }
}
